gtk-actors
==========

Experimental example of using an _actor model_ to create a _GTK_ GUI application with Rust.

Idea
----

The application consists of multiple _actors_ that communicate via _messages_.
The GTK UI is represented as an actor, other actors could be an _API client_ or a  _business logic backend_.

The GTK actor owns the GTK objects/widgets and should be very "thin".
Other actors are not talking to GTK directly, but send a _message_ to the GTK actor to interact with the UI.

The GTK actor runs in the main thread, with all the GTK code.
Each additional actor runs in a separate thread (using `futures::executor::ThreadPool`).
Actors internally communicate via channels (`futures::channel::mpsc`). 

Example Application
-------------------

The application consists of a label (`main_label`) and a button in the top left corner (`load_button`):

![](docs/screenshots/screenshot-initial.png)

When the button is clicked, the `UiActor` receives the message `LoadButtonClicked`, sets the label text accordingly, and asks the backend to load the data by sending a message:
```rust
impl ActorReceive<LoadButtonClicked> for UiActor {
    fn receive(&mut self, _msg: LoadButtonClicked, context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        self.widgets.main_label.set_text("Loading data...");
        self.backend_addr.send_ok(LoadExampleData { ui_addr: context.addr() });
        futures::future::ready(()).boxed()
    }
}
``` 

![](docs/screenshots/screenshot-loading.png)

When the `BackendActor` receives the `LoadExampleData` message, it loads the website "example.com", and sends back a message `SetMainLabel { text }` to the `UiActor` to update the label:
```rust
impl ActorReceive<LoadExampleData> for BackendActor {
    fn receive(&mut self, msg: LoadExampleData, _context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        let url = Uri::from_str("https://www.example.com").unwrap();
        fetch_url_text(url.clone())
            .then(move |result| {
                let text = match result {
                    Ok(text) => text,
                    Err(error) => format!("Failed to load '{}': {}", url, error)
                };
                msg.ui_addr.send_ok(SetMainLabel { text });
                futures::future::ready(())
            })
            .boxed()
    }
}
```

![](docs/screenshots/screenshot-loaded.png)
