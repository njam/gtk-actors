use std::future::Future;
use std::pin::Pin;

use futures::FutureExt;
use gtk::prelude::*;
use gtk::LabelExt;

use crate::actor::*;
use crate::app::*;
use crate::ui;

pub struct UiActor {
    widgets: ui::Widgets,
    backend_addr: ActorAddr<BackendActor>,
}

impl UiActor {
    pub fn new(
        widgets: ui::Widgets,
        backend_addr: ActorAddr<BackendActor>,
    ) -> Self {
        Self {
            widgets,
            backend_addr,
        }
    }
}

impl Actor for UiActor {
    fn started(&mut self, context: &mut ActorExecutionContext<Self>) {
        let addr = context.addr();
        self.widgets.load_button.connect_clicked(move |_| {
            addr.send_ok(LoadButtonClicked {});
        });
    }
}


pub struct LoadButtonClicked;

impl ActorReceive<LoadButtonClicked> for UiActor {
    fn receive(&mut self, _msg: LoadButtonClicked, context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        self.widgets.main_label.set_text("Loading data...");
        self.backend_addr.send_ok(LoadExampleData { ui_addr: context.addr() });
        futures::future::ready(()).boxed()
    }
}


pub struct SetMainLabel { pub text: String }

impl ActorReceive<SetMainLabel> for UiActor {
    fn receive(&mut self, msg: SetMainLabel, _context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        self.widgets.main_label.set_text(&msg.text);
        futures::future::ready(()).boxed()
    }
}
