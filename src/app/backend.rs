use std::error::Error;
use std::pin::Pin;
use std::str::FromStr;

use futures::prelude::*;
use http::{Request, Response, Uri};

use crate::actor::*;
use crate::app::*;

pub struct BackendActor {}

impl BackendActor {
    pub fn new() -> Self {
        Self {}
    }
}

impl Actor for BackendActor {}


pub struct LoadExampleData { pub ui_addr: ActorAddr<UiActor> }

impl ActorReceive<LoadExampleData> for BackendActor {
    fn receive(&mut self, msg: LoadExampleData, _context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()> + Send>> {
        let url = Uri::from_str("https://www.example.com").unwrap();
        fetch_url_text(url.clone())
            .then(move |result| {
                let text = match result {
                    Ok(text) => text,
                    Err(error) => format!("Failed to load '{}': {}", url, error)
                };
                msg.ui_addr.send_ok(SetMainLabel { text });
                futures::future::ready(())
            })
            .boxed()
    }
}

async fn fetch_url_text(url: Uri) -> Result<String, Box<dyn Error>> {
    let html = fetch_url(url).await?;
    let document = scraper::Html::parse_document(&html);
    let body = document
        .select(&scraper::Selector::parse("body").unwrap())
        .next().ok_or("Cannot find <body> tag".to_string())?;
    let body_text = body.text().collect::<Vec<&str>>().join("");
    Ok(body_text)
}

async fn fetch_url(url: Uri) -> Result<String, Box<dyn Error>> {
    let request = Request::builder()
        .uri(url)
        .method("GET")
        .body(())
        .unwrap();
    let response: Response<isahc::Body> = isahc::send_async(request).await?;
    let text = response.into_body().text()?;
    Ok(text)
}
