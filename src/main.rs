use gio::prelude::*;
use gtk::prelude::*;
use gtk::WidgetExt;

use actor::*;

use crate::app::*;

mod actor;
mod ui;
mod app;

fn main() {
    run_gtk();
}

fn run_gtk() {
    let app_id = "com.example.MyGtkActors";
    let gtk_app: gtk::Application = gtk::Application::new(Some(app_id), gio::ApplicationFlags::FLAGS_NONE)
        .expect("Cannot create GtkApplication");

    gtk_app.connect_activate(move |gtk_app| {
        if let Some(win) = gtk_app.get_active_window() {
            win.present();
        } else {
            start_app(gtk_app);
        }
    });

    gtk_app.run(&std::env::args().collect::<Vec<_>>());
}

fn start_app(gtk_app: &gtk::Application) {
    let widgets = ui::Widgets::new();
    let main_window = widgets.main_window();
    gtk_app.add_window(main_window);
    main_window.show_all();
    main_window.present();

    let mut glib_main_context = glib::MainContext::default();
    let mut actor_scheduler = ActorScheduler::new();

    let backend_actor = BackendActor::new();
    let backend_addr = actor_scheduler.spawn_in_thread(backend_actor);

    let ui_actor = UiActor::new(widgets, backend_addr.clone());
    let ui_addr = actor_scheduler.spawn_local_with(ui_actor, &mut glib_main_context);

    gtk_app.connect_shutdown(move |_| {
        backend_addr.terminate();
        ui_addr.terminate();
    });
}
