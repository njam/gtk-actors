use crate::ui::Builder;

pub struct Widgets {
    pub main_window: gtk::ApplicationWindow,
    pub main_label: gtk::Label,
    pub load_button: gtk::Button,
}

impl Widgets {
    pub fn new() -> Self {
        let builder = Builder::new(vec!(
            include_bytes!("../../res/resources.gresource")
        ));
        builder.load_ui("/com/example/MyGtkActors/ui/main_window.ui");
        builder.load_css("/com/example/MyGtkActors/app.css");

        Self {
            main_window: builder.get_object("app_window"),
            main_label: builder.get_object("main_label"),
            load_button: builder.get_object("load_button"),
        }
    }

    pub fn main_window(&self) -> &gtk::ApplicationWindow {
        &self.main_window
    }
}
